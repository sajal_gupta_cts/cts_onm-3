#source('C:/Users/talki/Desktop/INTERN CODES/[719]/[719] Data extract.R')
rm(list=ls(all =TRUE))

library(ggplot2)
library(gridExtra)

pathWrite <- "C:/Users/talki/Desktop/cec intern/results/719/"
result <- read.csv('C:/Users/talki/Desktop/cec intern/results/719/[719]_summary.csv')
dya <- "C:/Users/talki/Desktop/cec intern/results/719/[719] doubleyaxisplot1.pdf"
dyr <- "C:/Users/talki/Desktop/cec intern/results/719/[719] doubleyaxisratio.pdf"

#manually add in x axis labels for last graph (monthly)
monthslabel <- c("Oct","Nov",'Dec','Jan','Feb','Mar','Apr','May','Jun')

#new examples!
dualplot1 <- "C:/Users/talki/Desktop/cec intern/results/719/[719] dual plot 1.pdf"
dualplot2 <- "C:/Users/talki/Desktop/cec intern/results/719/[719] dual plot 2.pdf"

rownames(result) <- NULL
result <- data.frame(result)
#result <- result[-length(result[,1]),]
result <- result[,-1]
colnames(result) <- c("date","da","pts","gsi","tamb","hamb","ratio1","ratio2")
result[,1] <- as.Date(result[,1], origin = result[,1][1])
result[,2] <- as.numeric(paste(result[,2]))
result[,3] <- as.numeric(paste(result[,3]))
result[,4] <- as.numeric(paste(result[,4]))
result[,5] <- as.numeric(paste(result[,5]))
result[,6] <- as.numeric(paste(result[,6]))
result[,7] <- as.numeric(paste(result[,7]))
result[,8] <- as.numeric(paste(result[,8]))

LTA <-  1851 #1939
LTD <- 5.07  #5.31
date <- result[,1]
last <- tail(result[,1],1)    #date[length(date)] works
first <- date[1]   #date[1]

dagraph <- ggplot(result, aes(x=date,y=da))+ylab("Data Availability [%]")
da1 <- dagraph + geom_line(size=0.5)
da1 <- da1 + theme_bw()
da1 <- da1 + expand_limits(x=date[1],y=0)
da1 <- da1 + scale_y_continuous(breaks=seq(0, 115, 10))
da1 <- da1 + scale_x_date(date_breaks = "1 month",date_labels = "%b")
da1 <- da1 + ggtitle(paste0("[719] Cebu Data Availability"), subtitle = paste0("From ",date[1]," to ",last)) 
da1 <- da1 + theme(axis.title.y = element_text(size = 11, face = "bold",margin = margin(0,2,0,0)))
da1 <- da1 + theme(axis.title.x = element_blank())
da1 <- da1 + theme(axis.text.x = element_text(size=11))
da1 <- da1 + theme(axis.text.y = element_text(size=11))
da1 <- da1 + theme(plot.title = element_text(face = "bold",size= 12,lineheight = 0.7,hjust = 0.5), 
                   plot.subtitle = element_text(face = "bold",size= 12,lineheight = 0.9,hjust = 0.5))
da1 <- da1 + theme(plot.title=element_text(margin=margin(0,0,7,0)))
da1 <- da1 + annotate("text",label = paste0("Average data availability = ", round(mean(result[,2]),1),"%"),size = 3.7,
                      x = as.Date(date[round(0.5*length(date))]), y= 22.5)
da1 <- da1 + annotate("text",label = paste0("Lifetime = ", length(date)," days (",format(round(length(date)/365,1),nsmall = 1)," years)   "),size = 3.7,
                      x = as.Date(date[round(0.5*length(date))]), y= 16)
da1 <- da1 + theme(plot.margin = unit(c(0.2,0.5,1,0.1),"cm"))#top,right,bottom,left

da1

ggsave(da1,filename = paste0(pathWrite,"719_DA_LC.pdf"), width = 7.92, height = 5)


gsiGraph <- ggplot(result, aes(x=date,y=gsi))+ylab("GHI [W/m�]")
gs1 <- gsiGraph + geom_bar(stat = "identity", width = 1, position = "dodge")
gs1 <- gs1 + theme_bw()
gs1 <- gs1 + expand_limits(x=date[1],y=8)
gs1 <- gs1 + scale_y_continuous(breaks=seq(0, 8, 1))
gs1 <- gs1 + scale_x_date(date_breaks = "1 month",date_labels = "%b")
gs1 <- gs1 + ggtitle(paste("[719] Global Horizontal Irradiation Daily"), subtitle = paste0("From ",date[1]," to ",last)) 
gs1 <- gs1 + theme(axis.title.y = element_text(face = "bold",size = 11,margin = margin(0,2,0,0)))
gs1 <- gs1 + theme(axis.title.x = element_blank())
gs1 <- gs1 + theme(axis.text.x = element_text(size=11))
gs1 <- gs1 + theme(axis.text.y = element_text(size=11))
gs1 <- gs1 + theme(plot.title = element_text(face = "bold",size= 12,lineheight = 0.7,hjust = 0.5), 
                   plot.subtitle = element_text(face = "bold",size= 12,lineheight = 0.7,hjust = 0.5))
gs1 <- gs1 + theme(plot.title=element_text(margin=margin(0,0,7,0)))
gs1 <- gs1 + geom_hline(yintercept=LTD, size=0.3, color ="blue")
gs1 <- gs1 + annotate("text",label = paste0("Long-term average annual GHI = ", LTA ," kWh/m�   "), size=3.7,
                      x = as.Date(date[round(0.518*length(date))]), y= 8.3)
gs1 <- gs1 + annotate("text",label = paste0("Long-term average daily GHI = ", LTD, " kWh/m�.day"), size=3.7,
                      x = as.Date(date[round(0.518*length(date))]), y= 7.8, color="blue")   # typically "Long-term average daily GHI = ",LTD," kWh/m^2.day"
gs1 <- gs1 + annotate("text",label = paste0("Current average daily GHI = ",round(mean(result[,4]),2)," kWh/m�.day      "), size=3.7,
                      x = as.Date(date[round(0.518*length(date))]), y= 7.3, color="red")
gs1 <- gs1 + geom_hline(yintercept=round(mean(result[,4]),2), size=0.3, color="red")
gs1 <- gs1 + theme(plot.margin = unit(c(0.2,0.5,1,0.1),"cm"))

gs1

ggsave(gs1,filename = paste0(pathWrite,"719_GSI_LC.pdf"), width = 7.92, height = 5)

tamb <- as.numeric(paste(result[,5]))
hamb <- as.numeric(paste(result[,6]))
pdf(dya, width =7.92, height=5)
tamb[1:31] <- NA
hamb[1:31] <- NA #check raw data, data werent useful
tamb_mean <- mean(tamb,na.rm=TRUE)
hamb_mean <- mean(hamb,na.rm=TRUE)
yaxis <- tamb
yaxis2 <- hamb
xaxis <- c(1:length(yaxis))
xaxis2 <- seq(14,(length(yaxis)),25)  # here whats need to change if labels length differ error
b_unit <- expression(~degree~C) 

par(mar=c(3.5, 4, 3, 4) + 0.1)

## Plot first set of data and draw its axis
plot(xaxis, tamb, pch=4, axes=FALSE, ylim=c(-100,75), xlab="",ylab="", 
     type="l",col="orange", main=" ")
axis(2, ylim=c(-100,75),at = seq(-100,75,25),col="black",col.axis="black",las=1)
mtext(expression(bold("Tamb [ C]")),side=2,line=2.5,cex=1,las =3)
#text(-27,35,b_unit,xpd=NA,srt = 90, cex = 1.1)

box()

## Allow a second plot on the same graph
par(new=TRUE)

## Plot the second plot and put axis scale on right
plot(xaxis, hamb, pch=3,  xlab="", ylab="", ylim=c(-0,250), 
     axes=FALSE, type="l", col="blue")

axis(4, ylim=c(-0,250), col="black",col.axis="black",las=1)
text(length(xaxis)*1.15,125,expression(paste(bold("Hamb [%]"))),xpd=NA,srt = -90, cex = 1)

title(paste("[719] Daily Average Ambient Temperature and Relative Humidity \n from ", date[1], " to ", last), col="black", cex.main =1)

#mtext(expression(paste(bold("Daily Average Ambient Temperature and Relative Humidity"))),side=3,col="black",line=1.75,cex=1.2)  
#mtext(expression(paste(bold("from 2016-03-18 to ", last))),side=3,col="black",line=0.75,cex=1.2)  
text(length(xaxis)/2, 225, paste("Average Tamb =",format(round(tamb_mean,1),nsmall = 1),"C"),cex = .8,col = "orange")
text(length(xaxis)/2, 25, paste("Average Hamb =",format(round(hamb_mean,1),nsmall = 1),"%"),cex = .8,col = "blue")

axis(1, at = xaxis2, cex.axis = 0.65, labels = monthslabel)

legend(length(yaxis)*0.85,260,
       c("Tamb","Hamb"),
       lty=c(1,1), # gives the legend appropriate symbols (lines)
       lwd=c(2.5,2.5),col=c("orange","blue")) # gives the legend lines the correct color and width

par(new=TRUE)


dev.off()


ratio1 <- as.numeric(paste(result[,7]))
ratio2 <- as.numeric(paste(result[,8]))
ratio1[1:31] <- NA
ratio2[1:32] <- NA
pdf(dyr,width=7.92, height=5)
yaxis <- ratio1       #pyr/gsi00
yaxis2 <- ratio2      #gmod/gsi00

xaxis <- c(1:length(yaxis))
xaxis2 <- seq(14,(length(yaxis)),25)  #prev is 30.5/mnth
#b_unit <- expression(~degree~C) 

par(mar=c(3.5, 4, 3, 4) + 0.1)

## Plot first set of data and draw its axis
plot(xaxis, ratio1, pch=4, axes=FALSE, ylim=c(0,150), xlab="", ylab="", 
     type="l",col="blue", main=" ")
axis(2, ylim=c(0,150),at = seq(0,150,25),col="black",col.axis="black",las=1)
mtext(expression(bold("Pyr/Gsi00 Ratio [%]")),side=2,line=2.5,cex=1,las =3)
#text(-27,35,b_unit,xpd=NA,srt = 90, cex = 1.1)

box()

## Allow a second plot on the same graph
par(new=TRUE)

## Plot the second plot and put axis scale on right
plot(xaxis, ratio2, pch=3,  xlab="", ylab="", ylim=c(50,200), 
     axes=FALSE, type="l", col="orange")

axis(4, ylim=c(50,200), at = seq(-50,200,25),col="black",col.axis="black",las=1)
#text(length(xaxis)+1450,100,expression(paste(bold(""))),xpd=NA,srt = -90, cex = 1.2)
text(length(xaxis)*1.15,125,expression(paste(bold("Gmod/Gsi00 Ratio [%]"))),xpd=NA,srt = -90, cex = 1)

title(paste("[719] Pyr/Gsi00 & Gmod/Gsi00 Ratio Plot \n from ", date[1], " to ", last), col="black", cex.main =1)

text(length(xaxis)/2, 180, paste("Average Pyr/Gsi00 ratio =",format(round(mean(ratio1,na.rm=TRUE),1),nsmall =1),"%"),cex = .8,col="blue")
text(length(xaxis)/2, 60, paste("Average Gmod/Gsi00 ratio =",format(round(mean(ratio2, na.rm=TRUE),1),nsmall = 1),"%"),cex = .8,col = "orange")


axis(1, at = xaxis2, cex.axis = 0.65, labels = c("Oct","Nov",'Dec','Jan','Feb','Mar','Apr','May','Jun'))

legend(length(yaxis)*0.77,208,
       c("Pyr/Gsi00","Gmod/Gsi00"),# puts text in the legend
       lty=c(1,1), # gives the legend appropriate symbols (lines)
       lwd=c(2.5,2.5),col=c("blue","orange")) # gives the legend lines the correct color and width

par(new=TRUE)

dev.off()

### 31/1/2018 add in pyr/gsi00 and gmod/gsi00

#NEW VERSION OF DUAL DATA PLOT WITH GGPLOT

result[1:31,5:8] <- NA
result[32,8] <- NA

tamb_mean <- mean(result[,5],na.rm=TRUE)
hamb_mean <- mean(result[,6],na.rm=TRUE)

p3 <- ggplot() + theme_bw() + theme(panel.grid.minor = element_blank())
p3 <- p3 + ggtitle("[719] Daily Average Ambient Temperature and Relative Humidity", subtitle = paste0("From ", date[1], " to ", last))
p3 <- p3 + geom_line(data=result, aes(x=date , y =tamb), colour = "orange2")
p3 <- p3 + scale_y_continuous(breaks=seq(20, 30, 2))
p3 <- p3 + ylab("Tamb [ C]") + coord_cartesian(ylim = c(20,30))
p3 <- p3 + theme(plot.title = element_text(face = "bold",size= 12,lineheight = 0.7,hjust = 0.5), 
                 plot.subtitle = element_text(face = "bold",size= 12,lineheight = 0.7,hjust = 0.5),
                 axis.title.x = element_blank())
p3 <- p3 + annotate('text', label = paste0('Average Tamb = ', round(tamb_mean,1), '%'), x= date[length(date)*0.5], y = 22.7, colour = "orange2")

p4 <- ggplot() + theme_bw() + theme(panel.grid.minor = element_blank())
p4 <- p4 + geom_line(data=result, aes(x=date, y=hamb), colour = "blue")
p4 <- p4 + ylab("Hamb [%]") + coord_cartesian(ylim = c(60,100)) + xlab("Date")
p4 <- p4 + annotate('text', label = paste0('Average Hamb = ', round(hamb_mean,1), '%'), x= date[length(date)*0.5], y = 95.5, colour ="blue")

ggsave(dualplot1, arrangeGrob(p3, p4), width = 7.92, height = 6)

ratio1_mean <- mean(result[,7], na.rm = TRUE)
ratio2_mean <- mean(result[,8], na.rm = TRUE)

p5 <- ggplot() + theme_bw() + theme(panel.grid.minor = element_blank())
p5 <- p5 + ggtitle('[719] Pyr/Gsi00 & Gmod/Gsi00 Ratio Plot', subtitle = paste0("From ", date[1], " to ", last))
p5 <- p5 + geom_line(data=result, aes(x=date , y = ratio1), colour = "orange2")
p5 <- p5 + scale_y_continuous(breaks=seq(20, 110, 20))
p5 <- p5 + ylab("Pyr/Gsi00 [%]") + coord_cartesian(ylim = c(20, 110))
p5 <- p5 + theme(plot.title = element_text(face = "bold",size= 12,lineheight = 0.7,hjust = 0.5), 
                 plot.subtitle = element_text(face = "bold",size= 12,lineheight = 0.7,hjust = 0.5), 
                 axis.title.x = element_blank())
p5 <- p5 + annotate('text', label = paste0('Average Pyr/Gsi00 = ', round(ratio1_mean,1), '%'), x= date[length(date)*0.5], y = 52.5, colour = "orange2")

p6 <- ggplot() + theme_bw() + theme(panel.grid.minor = element_blank())
p6 <- p6 + geom_line(data=result, aes(x=date , y = ratio2), colour = "blue")
p6 <- p6 + scale_y_continuous(breaks=seq(60, 120, 20))
p6 <- p6 + ylab("Gmod/Gsi00 [%]") + coord_cartesian(ylim = c(60, 120)) + xlab("Date")
p6 <- p6 + theme(plot.title = element_text(face = "bold",size= 12,lineheight = 0.7,hjust = 0.5), 
                 plot.subtitle = element_text(face = "bold",size= 12,lineheight = 0.7,hjust = 0.5))
p6 <- p6 + annotate('text', label = paste0('Average Gmod/Gsi00 = ', round(ratio2_mean, 1), '%'), x= date[length(date)*0.5], y = 67.5, colour = "blue")

ggsave(dualplot2, arrangeGrob(p5, p6), width = 7.92, height = 6)
print(paste0("Graphs located @  ",pathWrite))