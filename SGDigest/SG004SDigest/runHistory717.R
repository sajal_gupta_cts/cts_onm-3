source('/home/admin/CODE/common/math.R')
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}
rf = function(x)
{
  return(format(round(x,2),nsmall=2))
}
rf1 = function(x)
{
  return(format(round(x,1),nsmall=1))
}
prepareSumm = function(dataread)
{
	APPENDWARNING <<- 0
	da = nrow(dataread)
	daPerc = round(da/14.4,1)
  thresh = 5/1000
	gsi1 =tsi1=NA
  
	dataread2 = dataread[complete.cases(dataread[,3]),3]
	if(length(dataread2))
		gsi1 = sum(dataread[complete.cases(dataread[,3]),3])/60000
#  gsi2 = sum(dataread[complete.cases(dataread[,4]),4])/60000
#  gsismp = sum(dataread[complete.cases(dataread[,5]),5])/60000
  subdata = dataread[complete.cases(dataread[,3]),]
  subdata = subdata[as.numeric(subdata[,3]) > thresh,]
	
	dataread2 = subdata[complete.cases(subdata[,4]),4]
	if(length(dataread2))
  tsi1 = mean(dataread2)
#  tsi2 = mean(dataread[complete.cases(dataread[,3]),9])
  
#  gsirat = gsi1 / gsi2
#  smprat = gsismp / gsi2

	Eac11 = Eac12 = Eac21 = Eac22 = PR11 = PR12 = PR21 = PR22 = NA
	
	dataread2 = dataread[complete.cases(dataread[,18]),18]
	if(length(dataread2))
	Eac11 = sum(dataread[complete.cases(dataread[,18]),18])/-60
	
	dataread2 = dataread[complete.cases(dataread[,65]),65]
	if(length(dataread2))
  Eac12 = sum(dataread[complete.cases(dataread[,65]),65])/-60
	
	LastR1=LastR2=LastT1=LastT2=NA
	dataread2 = as.numeric(dataread[complete.cases(dataread[,34]),34])
	print(paste('length(dataread2)',length(dataread2)))
	time = dataread[complete.cases(dataread[,34]),1]
	condn = (dataread2 > 0 & dataread2 < 10000000)
	dataread2 = dataread2[condn]
	time = time[condn]
	print(paste('length(dataread2) after condn',length(dataread2)))
	if(length(dataread2))
	{
	print('have length')
  Eac21 = as.numeric(dataread2[length(dataread2)])  - as.numeric(dataread2[1])
	LastR1 = as.numeric(dataread2[length(dataread2)])
	LastT1 = as.character(time[length(time)])
	}
	dataread2 = dataread[complete.cases(dataread[,81]),81]
	if(length(dataread2))
	{
		idxspurious = (as.numeric(dataread2) < 10000000)
		if(idxspurious[length(idxspurious)] == FALSE)
		{
			APPENDWARNING <<-1 
		}
		dataread2 = dataread2[as.numeric(dataread2) < 10000000] # Prevent spurious values
	}
	time = dataread[complete.cases(dataread[,81]),1]
	if(length(dataread2))
  {
	  Eac22 = as.numeric(dataread2[length(dataread2)])  - as.numeric(dataread2[1])
	  LastR2 = as.numeric(dataread2[length(dataread2)])
		LastT2 = as.character(time[length(time)])
	}
	PR11 = (Eac11 * 100) / (gsi1*384.00)
	PR12 = (Eac12 * 100) / (gsi1*460.80)
	PR21 = (Eac21 * 100) / (gsi1*384.00)
	PR22 = (Eac22 * 100) / (gsi1*460.80)
	YLD11 = 0.01*PR11 * gsi1
	YLD12 = 0.01*PR12 * gsi1
	YLD21 = 0.01*PR21 * gsi1
	YLD22 = 0.01*PR22 * gsi1
	FullSiteProd = Eac21 + Eac22
	FullSiteYld=(FullSiteProd / 844.8)
	dateAc = NA
	ylds = c(YLD21,YLD22)
	sddev = sdp(ylds)
	cov = sddev * 100 / mean(ylds)
	if(nrow(dataread))
		dateAc = substr(dataread[1,1],1,10)
  datawrite = data.frame(Date = dateAc,
	                       PtsRec = rf(da),
												 Gsi00 = rf(gsi1), 
                         TMod = rf1(tsi1),
												 Eac1MA = rf(Eac11),
												 Eac1MB = rf(Eac12),
												 Eac2MA = rf(Eac21),
												 Eac2MB = rf(Eac22),
												 PR1MA = rf1(PR11),
												 PR1MB = rf1(PR12),
												 PR2MA = rf1(PR21),
												 PR2MB = rf1(PR22),
												 FullSiteProd= rf(FullSiteProd),
												 FullSiteYld = rf(FullSiteYld),
												 YLD11 = rf(YLD11),
												 YLD12 = rf(YLD12),
												 YLD21 = rf(YLD21),
												 YLD22 = rf(YLD22),
												 LastR1=LastR1,
												 LastR2=LastR2,
												 SDYlds = rf(sddev),
												 CovYlds = rf1(cov),
												 LastT1 = LastT1,
												 LastT2 = LastT2,
												 DA=rf1(daPerc),
												 stringsAsFactors=F
												)
  datawrite
}
rewriteSumm = function(datawrite)
{
  
  df = datawrite
  df
}

path = "/home/admin/Dropbox/Cleantechsolar/1min/[717]"
pathwrite = "/home/admin/Dropbox/Second Gen/[SG-004S]"
checkdir(pathwrite)
years = dir(path)
x=y=z=1
for(x in 1 : length(years))
{
  pathyear = paste(path,years[x],sep="/")
  writeyear = paste(pathwrite,years[x],sep="/")
  checkdir(writeyear)
  months = dir(pathyear)
  for(y in  1: length(months))
  {
    pathmonth = paste(pathyear,months[y],sep="/")
    writemonth = paste(writeyear,months[y],sep="/")
    checkdir(writemonth)
    days = dir(pathmonth)
    sumfilename = paste("[SG-004S] ",substr(months[y],3,4),substr(months[y],6,7),".txt",sep="")
    for(z in 1 : length(days))
    {
      dataread = read.table(paste(pathmonth,days[z],sep="/"),sep="\t",header = T)
      datawrite = prepareSumm(dataread)
      datasum = rewriteSumm(datawrite)
			currdayw = gsub("717","SG-004S",days[z])
      write.table(datawrite,file = paste(writemonth,currdayw,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
      {
        if(!file.exists(paste(writemonth,sumfilename,sep="/")) || (x == 1 && y == 1 && z==1))
        {
          write.table(datasum,file = paste(writemonth,sumfilename,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
        }
        else 
        {
          write.table(datasum,file = paste(writemonth,sumfilename,sep="/"),append = T,sep="\t",row.names = F,col.names = F)
        }
      }
    }
  }
}
