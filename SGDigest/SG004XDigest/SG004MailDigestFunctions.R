rm(list = ls())
TIMESTAMPSALARM = NULL
TIMESTAMPSALARM2 = NULL
METERCALLED = 0
ltcutoff = .001
CABLOSSTOPRINT = 0
instcaps = c(384.0,460.84)
getGTIData = function(date)
{
	yr=unlist(strsplit(date,"-"))
	yrmon =paste(yr[1],yr[2],sep="-")
	yr = yr[1]
	path = paste("/home/admin/Dropbox/Second Gen/[SG-004S]/",yr,"/",
	yrmon,"/[SG-004S] ",date,".txt",sep="")
	GTI=NA
	Tot = NA
	if(file.exists(path))
	{
		dataread = read.table(path,header=T,sep="\t")
		GTI = as.numeric(dataread[1,3])
		Tot = as.numeric(dataread[1,13])
	}
	return(c(GTI,Tot))
}
timetomins = function(x)
{
  hr = unlist(strsplit(x,":"))
	seq1 = seq(from = 1,to = length(hr),by =2)
	seq2 = seq(from = 2,to = length(hr),by =2)
  min = as.numeric(hr[seq2]) 
  hr = as.numeric(hr[seq1]) * 60
  return((hr + min + 1))
}
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}
secondGenData = function(filepath,writefilepath)
{
  {
		if(METERCALLED == 1){
	 		TIMESTAMPSALARM <<- NULL}
		else if(METERCALLED == 2){
			TIMESTAMPSALARM2 <<- NULL}
	}
	print(paste('IN 2G',filepath))
  dataread = read.table(filepath,header = T,sep = "\t",stringsAsFactors=F)
	dataread2 = dataread

	lasttime=lastread=Eac2=NA
	
	coleac1 = 47
	coleac2 = 12
#	if(METERCALLED == 3)
#		coleac2 = 12
	if(ncol(dataread) < 49){
		coleac1 = 15
		coleac2 = 39}
		dataread = dataread2[complete.cases(as.numeric(dataread2[,coleac2])),]
	if(nrow(dataread)>0)
	{
	lasttime = as.character(dataread[nrow(dataread),1])
	lastread = as.character(round(as.numeric(dataread[nrow(dataread),coleac2])/1000,3))
  Eac2 = format(round((as.numeric(dataread[nrow(dataread),coleac2]) - as.numeric(dataread[1,coleac2]))/1000,2),nsmall=1)
	}
	dataread = dataread2[complete.cases(as.numeric(dataread2[,coleac1])),]
	Eac1 = RATIO=NA
	if(nrow(dataread) > 1)
	{
   	Eac1 = format(abs(round(sum(as.numeric(dataread[,coleac1]))/12000,1)),nsmall=2)
		RATIO = round(as.numeric(Eac1)*100/as.numeric(Eac2),1)
	}
	dataread = dataread2
  DA = format(round(nrow(dataread)/14.4,1),nsmall=1)
  tdx = timetomins(substr(dataread[,1],12,16))
  dataread2 = dataread[tdx > 540,]
  tdx = tdx[tdx > 540]
  dataread2 = dataread2[tdx < 1020,]
	dataread2 = dataread2[complete.cases(as.numeric(dataread2[,coleac1])),]
  missingfactor = 480 - nrow(dataread2)
	{
		if(METERCALLED==1)
  		dataread2 = dataread2[abs(as.numeric(dataread2[,coleac1])) < ltcutoff,]
		else
  		dataread2 = dataread2[abs(as.numeric(dataread2[,coleac1])) < ltcutoff,]
	}
	if(length(dataread2[,1]) > 0)
	{
		{
			if(METERCALLED == 1){
				TIMESTAMPSALARM <<- as.character(dataread2[,1])}
			else if(METERCALLED == 2){
		 	 TIMESTAMPSALARM2 <<- as.character(dataread2[,1])}
		}
	}
  totrowsmissing = format(round((missingfactor + nrow(dataread2))/4.8,1),nsmall=1)
	date = NA
	if(nrow(dataread)>0)
		date = substr(as.character(dataread[1,1]),1,10)
	print(paste('Date',date))
	print(paste('EAC1',Eac1))
	print(paste('Eac2',Eac2))
	print(paste('RATIO',RATIO))
	print(paste('DA',DA))
	print(paste('Downtime',totrowsmissing))
	print(paste('Lasttime',lasttime))
	print(paste('LastRead',lastread))
	DSPY1 = round((as.numeric(Eac1)/instcaps[METERCALLED]),2)
	DSPY2 = round((as.numeric(Eac2)/instcaps[METERCALLED]),2)
	GTI = getGTIData(date)
	Tot = GTI[2]
	GTI = GTI[1]
	PR1 = round(DSPY1*100/GTI,1)
	PR2 = round(DSPY2*100/GTI,1)
	{
	  if(METERCALLED == 1)
	  {
      df = data.frame(Date = date, SolarPowerMeth1 = as.numeric(Eac1),SolarPowerMeth2 = as.numeric(Eac2),
                  Ratio=RATIO,DA = DA,Downtime = totrowsmissing,
									LastTime = lasttime, LastRead = lastread,Yield1 = DSPY1, Yield2 = DSPY2,GSI=GTI, PR1=PR1, PR2=PR2,TotEnSG004S=Tot,stringsAsFactors = F)
    } 
	  else if(METERCALLED == 2)
	  {
      df = data.frame(Date = date, SolarPowerMeth1 = as.numeric(Eac1),SolarPowerMeth2= as.numeric(Eac2),
                  Ratio=RATIO,DA = DA,Downtime = totrowsmissing,
									LastTime = lasttime, LastRead = lastread,Yield1 = DSPY1,Yield2 = DSPY2, GSI=GTI, PR1=PR1, PR2=PR2,TotEnSG004S=Tot,stringsAsFactors = F)
	  }
	}
	write.table(df,file = writefilepath,row.names = F,col.names = T,sep="\t",append = F)
  return(df)
}
thirdGenData = function(filepathm1, filepathm2,writefilepath)
{
  dataread2 =read.table(filepathm1,header = T,sep="\t",stringsAsFactors=F) #its correct dont change
  dataread1 = read.table(filepathm2,header = T,sep = "\t",stringsAsFactors=F) #its correct dont change
	dt = as.character(dataread1[,6])
	da = as.character(dataread1[,5])
	EacCokeMeth1 = as.numeric(dataread2[,2])
	EacCokeMeth2 = as.numeric(dataread2[,3])
	CokeLoadMeth1=ArtificialLoad=CokeLoadMeth2=PercentageArtSolar=RatioCokeLoad=NA
	ltCokeLoad=lrCokeLoad=DailyCons=NA
	SolarEnergyMeth1 = as.numeric(dataread1[,2])
	SolarEnergyMeth2 = as.numeric(dataread1[,3])
  RatioCoke = as.numeric(dataread2[,4])
  RatioSolar = as.numeric(dataread1[,4])
	CABLOSS= round((EacCokeMeth2/SolarEnergyMeth2),3)
	CABLOSS = abs(1 - CABLOSS)*100
	CABLOSSTOPRINT <<- format(CABLOSS,nsmall=2)
	ltSol = as.character(dataread1[,7])
	lrSol = as.character(dataread1[,8])
	ltCoke = as.character(dataread2[,7])
	lrCoke = as.character(dataread2[,8])
	GSI = as.character(dataread1[,11])
	PRM11 = as.character(dataread1[,12])
	PRM12 = as.character(dataread1[,13])
	PRM21 = as.character(dataread1[,12])
	PRM22 = as.character(dataread1[,13])
	Tot = as.character(dataread1[,14])
	df = data.frame(Date = substr(as.character(dataread1[1,1]),1,10),
	DA= da,
	Downtime = dt,
	EacCokeMeth1 = EacCokeMeth1,
	EacCokeMeth2 = EacCokeMeth2,
	SolarEnergyMeth1=SolarEnergyMeth1,
	SolarEnergyMeth2=SolarEnergyMeth2,
	RatioCoke=RatioCoke,
	RatioSolar=RatioSolar,
	CableLoss=CABLOSS,
	LastTimeSol=ltSol,
	LastReadSol=lrSol,
	LastTimeCoke=ltCoke,
	LastReadCoke=lrCoke,
	GSI=GSI,
	PRM11 = PRM11,
	PRM12 = PRM12,
	PRM21 = PRM21,
	PRM22 = PRM22,
	TotEnergySG004S=Tot,
	#ArtificialLoad=ArtificialLoad,
	#PercentageSolar=PercentageArtSolar,
	#DailyCons = DailyCons,
	stringsAsFactors=F)
  {
    if(file.exists(writefilepath))
    {
      write.table(df,file = writefilepath,row.names = F,col.names = F,sep = "\t",append = T)
    }
    else
    {
      write.table(df,file = writefilepath,row.names = F,col.names = T,sep = "\t",append = F)
    }
  }
}
