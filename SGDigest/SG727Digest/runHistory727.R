rm(list = ls(all = T))
source('/home/admin/CODE/common/math.R')
INSTCAP = 486.80
DAYSACTIVE = 0
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}
rf1 = function(x)
{
  return(format(round(x,1),nsmall=1))
}
rf = function(x)
{
  return(format(round(x,2),nsmall=2))
}

fetchGSIData = function(date,wait)
{
  pathMain = '/home/admin/Dropbox/Second Gen/[SG-724S]'
  yr = substr(date,1,4)
  mon = substr(date,1,7)
  txtFileName = paste('[SG-727S] ',date,".txt",sep="")
  print(txtFileName)
  pathyr = paste(pathMain,yr,sep="/")
  gsiVal = NA
  if(file.exists(pathyr))
  {
    pathmon = paste(pathyr,mon,sep="/")
    if(file.exists(pathmon))
    {
      pathfile = paste(pathmon,txtFileName,sep="/")
      if(file.exists(pathfile))
      {
        dataread = read.table(pathfile,sep="\t",header = T)
        gsiVal = as.numeric(dataread[,3])
      }
    }
  }
  if(wait && (!is.finite(gsiVal)))
  {
    print("Couldnt find file, sleeping for an hour")
    Sys.sleep(3600)
    gsiVal = fetchGSIData(date,0)
  }
  return(gsiVal)
}

prepareSumm = function(dataread,wait)
{
  da = nrow(dataread)
  daPerc = round(da/14.4,1)
  thresh = 5
  LastRead = LastTime = Eac1 = Eac2 = Eac3 = Eac4=NA
  
  date = substr(as.character(dataread[1,1]),1,10)
  Eac1 = as.numeric(dataread[,16])
  Eac1 = Eac1[complete.cases(Eac1)]
  
  {
    if(length(Eac1))
      Eac1 = sum(Eac1)/60
    else
      Eac1 = NA
  }
  
  Eac2 = as.numeric(dataread[,31])
  time = as.character(dataread[,1])
  time = time[complete.cases(Eac2)]
  Eac2 = Eac2[complete.cases(Eac2)]
  {
    if(length(Eac2))
    {
      LastRead = Eac2[length(Eac2)]
      Eac2 = (Eac2[length(Eac2)] - Eac2[1])
      LastTime = time[length(time)]
    }
    else
      Eac2 = NA
  }
  
  Eac3 = as.numeric(dataread[,32])
  Eac3 = Eac3[complete.cases(Eac3)]
  {
    if(length(Eac3))
      Eac3 = Eac3[length(Eac3)]
    else
      Eac3 = NA
  }
  
  Eac4 = as.numeric(dataread[,34])
  Eac4 = Eac4[complete.cases(Eac4)]
  {
    if(length(Eac4))
      Eac4 = Eac4[length(Eac4)]
    else
      Eac4 = NA
  }
  
  Yld1 = Eac1/INSTCAP
  Yld2 = Eac2/INSTCAP
  IrrSG724 = fetchGSIData(date,wait)
  PR1 = Yld1*100/IrrSG724
  PR2 = Yld2*100/IrrSG724
  
  datawrite = data.frame(Date = date, PtsRec = da, DA = rf1(daPerc), Eac1 = rf(Eac1), Eac2 = rf(Eac2),
                         Yld1 = rf(Yld1), Yld2 = rf(Yld2), GSiSG724 = IrrSG724, PR1 = rf1(PR1), PR2 = rf1(PR2),
                         LastTime = LastTime, LastRead = LastRead, LastReadRec = Eac3, LastReadRecDel = Eac4, stringsAsFactors=F)
  
  datawrite 
}

rewriteSumm = function(datawrite)
{
  datawrite
}

path = "/home/admin/Dropbox/Cleantechsolar/1min/[727]"
pathwrite = "/home/admin/Dropbox/Second Gen/[SG-727S]"
checkdir(pathwrite)
years = dir(path)
x=y=z=1
for(x in 1 : length(years))
{
  pathyear = paste(path,years[x],sep="/")
  writeyear = paste(pathwrite,years[x],sep="/")
  checkdir(writeyear)
  months = dir(pathyear)
  for(y in  1: length(months))
  {
    pathmonth = paste(pathyear,months[y],sep="/")
    writemonth = paste(writeyear,months[y],sep="/")
    checkdir(writemonth)
    days = dir(pathmonth)
    sumfilename = paste("[SG-727S] ",substr(months[y],3,4),substr(months[y],6,7),".txt",sep="")
    for(z in 1 : length(days))
    {
      dataread = read.table(paste(pathmonth,days[z],sep="/"),sep="\t",header = T)
      datawrite = prepareSumm(dataread,0) #dont wait for history
      datasum = rewriteSumm(datawrite)
      currdayw = gsub("727","SG-727S",days[z])
      if(x == 1 && y == 1 && z == 1)
        DOB = substr(days[z],10,19)
      DAYSACTIVE = DAYSACTIVE + 1
      write.table(datawrite,file = paste(writemonth,currdayw,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
      {
        if(!file.exists(paste(writemonth,sumfilename,sep="/")) || (x == 1 && y == 1 && z==1))
        {
          write.table(datasum,file = paste(writemonth,sumfilename,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
        }
        else 
        {
          write.table(datasum,file = paste(writemonth,sumfilename,sep="/"),append = T,sep="\t",row.names = F,col.names = F)
        }
      }
    }
  }
}

