rm(list =ls())

path = "/home/admin/Data/Episcript-CEC/EGX PH006X Dump"

days = dir(path)

for(x in 1 : length(days))
{
	if(grepl("PH-006",days[x]) && (!grepl("PH-006X",days[x])))
	{
		oldname = paste(path,days[x],sep="/")
		newname = paste(path,gsub("PH-006","PH-006X",days[x]),sep="/")
		print(paste("Replacing",oldname,"to",newname))
		file.rename(from=oldname,to=newname)
	}
}
