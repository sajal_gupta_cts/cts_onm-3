rm(list = ls())

path = "/home/admin/Dropbox/Gen 1 Data/[TH-002X]"

years = dir(path)

for(x in 1: length(years))
{
	pathyr = paste(path,years[x],sep="/")
	mons = dir(pathyr)
	for(y in 1 : length(mons))
	{
		pathstn = paste(pathyr,mons[y],sep="/")
		stns = dir(pathstn)
		for(z in 1 : length(stns))
		{
			pathdays = paste(pathstn,stns[z],sep="/")
			days = dir(pathdays)
			for(t in 1 : length(days))
			{
				oldname = paste(pathdays,days[t],sep="/")
				newname = gsub("TH-002XXX","TH-002X",days[t])
				newname=paste(pathdays,newname,sep="/")
				print(paste(oldname,"->",newname))
				file.rename(from=oldname,to=newname)
			}
		}
	}
}
